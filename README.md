# Activité formulaires Django

## Recherche d'informations

- Sous quelle forme se présente un formulaire HTML ?

- Comment fonctionne un formulaire HTML ?

- Donner des exemples d'élèments que l'on retrouve dans les formulaires HTML ?

- Où est spécifié l'adresse où sont envoyées les données du formulaire ?

- Qu'est-ce que l'attaque informatique de type CSRF ?

## Enoncé

L'objectif de cette activité est de manipuler les formulaires Django. Pour cela, nous allons créer une plateforme de questions / réponses avec Django. Nous allons utiliser 2 formulaires différents :

- formulaire pour l'ajout de nouvelles questions

- formulaire pour envoyer un email au créateur de l'application

## Partie 1 : mise en place de l'application Django

- Créer un dossier pour cette activité

- Créer un environnement virtuel et y installer django

- Créer un projet Django nommé "app_django" (https://docs.djangoproject.com/en/3.1/intro/tutorial01/)

- Créer une application nommée "questions" et vérifier sa création. Ajouter l'application dans le fichier `settings.py` dans `INSTALLED_APPS` (https://docs.djangoproject.com/en/3.1/intro/tutorial01/). Ajouter aussi 'crispy_forms' dans `INSTALLED_APPS` (pour la gestion de Bootstrap par les formulaires)

- Créer 6 vues : une pour la page d'accueil, une pour enregistrer des questions, une pour poser des questions, une pour les mauvaises réponses, une pour les bonnes réponses, une pour les réclamations (https://docs.djangoproject.com/en/3.1/intro/tutorial03/)

- Créer les urls pour ces 6 vues (https://docs.djangoproject.com/en/3.1/intro/tutorial03/)

- Dans l'application "questions", créer un dossier "templates" puis dans ce dossier créer un dossier "questions". Dans ce dossier, copier les templates présents dans ce dépôt (`base.html`, `home.html`, `pas_content.html`, `right.html`, `wrong.html`, `storage.html` et `question.html`)

- Connecter les liens vers les différentes pages dans le template `base.html` 

- Lancer le serveur de développement et vérifier son fonctionnement. Vérifier les différents liens.

## Partie 2 : formulaire d'ajout de questions

- Dans le fichier `models.py` de l'application, créer une classe nommée "Question" avec comme variables "votre_question", "bonne_reponse", "mauvaise_reponse_1" et "mauvaise_reponse_2"

- Effectuer les migrations suite à cette mise à jour du modèle (https://docs.djangoproject.com/en/3.1/intro/tutorial02/)

- Ajouter le lien vers le modèle créé dans le fichier `admin.py`

- Vérifier que vous pouvez modifier les données de la table à partir de l'interface d'administration (vous devez créer en amont un compte de superutilisateur)

- Dans le dossier de l'application, créer un fichier `forms.py` et créer une classe "QuestionForm" basée sur la classe créée dans le fichier `models.py` (remplir le code de la classe Meta)

```python
class QuestionForm(ModelForm):
    class Meta:
        # A REMPLIR
    
    def __init__(self, *args, **kwargs):
        super(QuestionForm, self).__init__(*args, **kwargs)
        for field in iter(self.fields):
            self.fields[field].widget.attrs.update({
                'class': 'form-control'
        })
```

- Intégrer le formulaire dans le fichier `view.py` dans la vue "storage" comme ceci :

```python
def storage(request):
    if request.method == 'POST':
        form = QuestionForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect('home')
    else:
        form = QuestionForm()
    return render(request, 'questions/storage.html', {'form': form})
```

- Vérifier que le formulaire fonctionne et que les données sont bien enregistrées dans la base de données

## Partie 3 : lecture des questions

- Modifier la vue pour les questions avec le code suivant :

```python
def question(request):
    the_question = Question.objects.order_by('?').first()
    answers = [
        (the_question.bonne_reponse, "right"),
        (the_question.mauvaise_reponse_1, "wrong"),
        (the_question.mauvaise_reponse_2, "wrong"),
    ]
    shuffle(answers)
    context = {
        "question" : the_question.votre_question,
        "answers" : answers,
    }
    return render(request, 'questions/question.html', context=context)
```

- Mettre à jour les templates des bonnes et des mauvaises réponses pour afficher une image ou un gif

- Vérifier le bon fonctionnement des questions

## Partie 4 : formulaire de mécontentement

- Créer un formulaire (non lié à un modèle Django) de mécontentement à partir de cet exemple : https://docs.djangoproject.com/en/3.1/topics/forms/#more-on-fields

- Utiliser des noms des champs en français (ce sont les noms qui apparaissent sur le formulaire HTML) en cohérence avec la vue "pas_content"

- Mettre à jour la vue "pas_content" avec le code suivant :

```python
def pas_content(request):
    if request.method == 'POST':
        form = ContactForm(request.POST)
        if form.is_valid():
            subject = form.cleaned_data['titre_de_votre_mecontentement']
            message = form.cleaned_data['votre_message']
            sender = form.cleaned_data['votre_email']
            cc_myself = form.cleaned_data['etre_en_copie']
            # ------ A METTRE à JOUR ---------
            recipients = ['votre_email_gmail']
            # --------------------------------
            if cc_myself:
                recipients.append(sender)
            send_mail(subject, message, sender, recipients)
            return redirect('home')
    else:
        form = ContactForm()
    return render(request, 'questions/pas_content.html', {'form': form})
```

- Mettre à jour le fichier `settings.py` avec le code suivant :

```python
EMAIL_BACKEND = 'django.core.mail.backends.smtp.EmailBackend'
EMAIL_HOST = 'smtp.gmail.com'
EMAIL_USE_TLS = True
EMAIL_PORT = 587
EMAIL_HOST_USER = 'votre_email_gmail'
EMAIL_HOST_PASSWORD = os.getenv("EMAIL_PWD")
```

**ATTENTION : il faut utiliser une variable d'environnement pour le mot de passe pour qu'il ne soit pas stocké en clair dans le fichier settings.py** 

- Modifier les paramètres de sécurité de votre compte Gmail pour activer les "Accès moins sécurisé des applications"

**ATTENTION : pour plus de sécurité sur votre compte Gmail, vous pouvez désactiver ces accès après cet exercice** 

- Vérifier que l'envoi du formulaire par email fonctionne correctement

## Ressources

- utilisation des formulaires https://docs.djangoproject.com/fr/3.1/topics/forms/
